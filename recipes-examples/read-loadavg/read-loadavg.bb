LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = "file://read_loadavg.sh"

inherit useradd

# See meta-skeleton/recipes-skeleton/useradd/useradd-example.bb for examples
# Needed when inheriting useradd
USERADD_PACKAGES = "${PN}"

# Add a group
GROUPADD_PARAM:${PN} = "monitoring"

# Create a user that will run our script
USERADD_PARAM:${PN} = "-s /bin/sh -g monitoring monitoring"

FILES:${PN} += "${datadir}/*"

do_configure () {
}

do_compile () {
}

do_install () {
	install -d ${D}/usr/bin
	install ${WORKDIR}/read_loadavg.sh ${D}/usr/bin/

# The user is already created at this stage
	install -d -m 700 ${D}${datadir}/monitoring
	chown -R monitoring ${D}${datadir}/monitoring
}

