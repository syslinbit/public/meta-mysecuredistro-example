#!/bin/sh
# A small example of a monitoring deamon
# License: MIT
# Author: Marta Rybczynska <marta.rybczynska@syslinbit.com>

MAX_ENTRIES=100

# Check if a file path is provided as argument
if [ -z "$1" ]; then
    echo "Usage: $0 <output_file>"
    exit 1
fi

LOG_FILE="$1"

# Function to read load average and save to log file
read_loadavg() {
    while true; do
        echo "$(date +'%Y-%m-%d %H:%M:%S') $(cat /proc/loadavg)" >> "$LOG_FILE"
        # Trim log file to keep only the last MAX_ENTRIES lines
        tail -n $MAX_ENTRIES "$LOG_FILE" > "$LOG_FILE.tmp" && mv "$LOG_FILE.tmp" "$LOG_FILE"
        sleep 30
    done
}

# Main
read_loadavg
