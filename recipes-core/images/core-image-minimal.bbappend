inherit extrausers

IMAGE_INSTALL:append = " sudo"

#printf "%q" $(mkpasswd -m sha256crypt where-is-the-password)
PASSWD = "\$5\$iqTmAn1pZ/ux.NTH\$J3/3FaGWu3YgJD1DBPPvBdczXHsEruHVFROkdwSC7p."

# Add user nonroot that belongs to sudo group
EXTRA_USERS_PARAMS = "\
    useradd -p '${PASSWD}' nonroot; \
    usermod -a -G sudo nonroot; \
    "

# Add sudo access to users in the sudo group
update_sudoers(){
    # Uncomment the line adding the sudo group to sudoers
    sed -i 's/^#\s*\(%sudo\s*ALL=(ALL:ALL)\s*ALL\)/\1/'  ${IMAGE_ROOTFS}/etc/sudoers
}

ROOTFS_POSTPROCESS_COMMAND += "update_sudoers; "
